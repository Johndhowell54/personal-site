#!/bin/python
import os
import fileinput
from datetime import date
# Vars to prevent magic numbers
title_line = 24
title_newline = 23
insert_title_line = 24
post_line = 30
rss_line = 13
rss_desc = []
blog_update_line = 31
def copy_template(post_title):
        os.system("cp template.html " + post_title + ".html")

def make_post():
	title = input("Enter blog post title: ")
	file_title = title.replace(" ", "_")
	copy_template(file_title)
	file = file_title + ".html"

	with open(file, 'r') as f:
		data = f.readlines()
	data[title_line] = "<h1>" + title + "</h1>\n"
	print("what title line should be: " + data[title_line])
	data[post_line] = "<p>" + input("Enter first line of post contents: ") + "</p>\n"        
	rss_desc.append(data[post_line])
	i = post_line + 1
	r = 0
	mult_line = input("More? [Y/N] ")
	
	if(mult_line == "Y" or mult_line == "y"):
		while(mult_line == "Y"):
			data[i] = "<p>" + input("Enter next line of post contents: ") + "</p>\n"
			rss_desc.append(data[i])
			i += 1
			r+= 1
			mult_line = input("More? [Y/n] ")

	with open(file, 'w') as f:
		f.writelines(data)
	insert_date(title)
	update_rss(title, rss_desc)


def insert_date(title):
	file = "blog.html"
	with open(file, 'r') as f:
		data = f.readlines()
	post_date = date.today().strftime("%B %d")
	data.insert(blog_update_line,"")
	data[blog_update_line] = "<li><a href=" + title + ".html" + "> " + post_date + ": " + title + "</a></li>\n"
	
	with open(file, 'w') as f:
		f.writelines(data)
	print("wrote to blog.html")

def update_rss(title, rss):
	rss_write = ''
	for line in rss:
		rss_write += line
	print("rss write: " + rss_write)
	file = "../rss.xml"
	with open(file, 'r') as f:
		data = f.readlines()
	post_date = date.today().strftime("%B %d")
	data.insert(rss_line,"")
	data[rss_line] = "<item>\n <title> " + title + " </title>\n <guid> https://www.johnhowell.online/Blogs/" + title +".html </guid>\n <pubDate> "+ post_date + " </pubDate>\n <description> "+ rss_write + "</description> </item>\n"
	with open(file, 'w') as f:
		f.writelines(data)

def main():
	make_post()
	#os.system("date '+%B %d' ")
main()
